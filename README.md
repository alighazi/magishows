**Magine TV**

This is a basic implementation of the assignment. It uses the regular stack of libraries for modern android develoment. Basically all the work in the app is done by the libs, I've done only a bit of plumbing. The libs are as follows:

- `androidx.appcompat`: Androidx compatiblity lib
- `constraintlayout`: Constraint layout for easier layouting
- `recyclerview`: Recycler view for performant lists
- `cardview`: A ui element that mimic the appreance of cards 
- `rxbinding`: RxJava binding APIs for Android's UI widgets. 
- `picasso`: Loading images from web
- `retrofit`: Rest api helper
- `retrofit2:converter-gson`: Middle man between retrofit and gson
- `okhttp3`: Http library
- `gson`: Json serialization lib
- `retrofit2:adapter-rxjava`: Rxjava adapter for retrofit
- `rxjava`: Reactive programming for java
- `rxandroid`: Reactive programming for android

The program is by no means fully finished at this current stage, one could add some UI tests and Unit tests for the added peace of mind, however I would not bother with writing tests for a project at this scale.



