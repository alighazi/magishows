package com.example.maginetest

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.maginetest.api.ServiceBuilder
import com.example.maginetest.api.ShowSearchResult
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val discard = RxTextView.afterTextChangeEvents(searchET)
            .skipInitialValue()
            .debounce(400, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {initiateSearch()}
    }

    private fun onComplete() {
        progressBar.visibility = View.GONE
    }

    private fun onFailure(t: Throwable) {
        retryTV.visibility = View.VISIBLE
    }

    private fun onResponse(response: List<ShowSearchResult>) {
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = ShowAdapter(response)
        }
    }

    fun retryClicked(view: View) {
        retryTV.visibility = View.GONE
        initiateSearch()
    }

    private fun initiateSearch(){
        progressBar.visibility = View.VISIBLE
        val compositeDisposable = CompositeDisposable()
        compositeDisposable.add(
            ServiceBuilder.buildService().searchShows(searchET.text.toString())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({response -> onResponse(response)}, {t -> onFailure(t) },
                    {onComplete()}))
    }
}