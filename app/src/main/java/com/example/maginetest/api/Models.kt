package com.example.maginetest.api

data class Image(
    val medium: String,
    val original: String
)

data class Show(
    val id: Int,
    val image: Image?,
    val name: String,
    val url: String
)

data class ShowSearchResult(
    val score: Double,
    val show: Show
)