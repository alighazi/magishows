package com.example.maginetest.api

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MagineTvEndPoints {

    @GET("/search/shows")
    fun searchShows(@Query("q") query: String): Observable<List<ShowSearchResult>>
}