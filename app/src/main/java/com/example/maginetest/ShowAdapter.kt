package com.example.maginetest

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.maginetest.api.ShowSearchResult
import com.squareup.picasso.Picasso

class ShowAdapter(private val show: List<ShowSearchResult>): RecyclerView.Adapter<ShowViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.show_item, parent, false)
        return ShowViewHolder(view)
    }

    override fun getItemCount(): Int {
        return show.size
    }

    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        return holder.bind(show[position])
    }
}

class ShowViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){
    private val imgView:ImageView = itemView.findViewById(R.id.showIV)
    private val title:TextView = itemView.findViewById(R.id.movie_title)

    fun bind(show: ShowSearchResult) {
        title.text = show.show.name
        if(show.show.image !=null) {
            Picasso.get().load(show.show.image.original).into(imgView)
        }else{
            imgView.setImageResource(android.R.drawable.stat_notify_error)
        }
    }

}
